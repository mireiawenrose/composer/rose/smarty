# Rose Framework Session
Smarty support plugin.

## Requirements
- PHP 8.3
- ext-gettext

## Features
Adds the Smarty templating support to the Rose Framework.
