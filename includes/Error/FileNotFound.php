<?php
declare(strict_types = 1);

namespace Rose\Framework\Smarty\Error;

use RuntimeException;
use Throwable;
use function _;
use function sprintf;

/**
 * This class extends the RuntimeException class and represents an exception that is thrown when a file is not found.
 *
 * @package Rose\Framework\Smarty\Error
 */
class FileNotFound extends RuntimeException
{
	public function __construct(string $message = '', int $code = 0, ?Throwable $previous = NULL)
	{
		$message = sprintf(_('Template file "%s" was not found'), $message);
		
		parent::__construct($message, $code, $previous);
	}
}