<?php
declare(strict_types = 1);

namespace Rose\Framework\Smarty;

use Rose\Framework\Core;
use Rose\Framework\ModuleInterface;
use Rose\Framework\Smarty\Error\FileNotFound;
use RuntimeException;
use Smarty;
use Smarty_Internal_Template;
use SmartyException;
use function _;
use function sprintf;

/**
 * Smarty module for Rose framework
 *
 * @package Rose\Framework\Smarty
 */
class SmartyModule implements ModuleInterface
{
	/**
	 * Module name
	 *
	 * @var string
	 */
	public const string MODULE_NAME = 'Smarty';
	
	/**
	 * The framework core
	 *
	 * @var Core
	 */
	protected Core $core;
	
	/**
	 * The instance of the Smarty itself
	 *
	 * @var Smarty
	 */
	protected Smarty $smarty;
	
	/**
	 * Get the name of the module
	 *
	 * @return string
	 */
	public function GetName() : string
	{
		return self::MODULE_NAME;
	}
	
	/**
	 * Construct the object
	 *
	 * @param Core $core
	 *    The framework core
	 */
	public function __construct(Core $core)
	{
		$this->core = $core;
		
		// Check Smarty cache path
		$cache = $this->core->Configuration()->GetString('smarty_template_cache_path', sprintf('%s/%s', __DIR__, 'cache'));
		
		if ((!is_dir($cache)) || (!is_writable($cache)))
		{
			throw new RuntimeException(sprintf(_('Smarty template cache path "%s" was not found, was not directory or is not writable'), $cache));
		}
		
		// Check for Smarty template path
		$templates = $this->core->Configuration()->GetString('smarty_template_path', sprintf('%s/%s', __DIR__, 'templates'));
		
		if ((!is_dir($templates)) || (!is_readable($templates)))
		{
			throw new RuntimeException(sprintf(_('Smarty template path "%s" was not found, was not directory or is not readable'), $templates));
		}
		
		// Initialize the Smarty
		$this->smarty = new Smarty();
		
		// Set up paths
		$this->smarty->setTemplateDir([$templates]);
		$this->smarty->setCompileDir($cache);
		$this->smarty->setCacheDir($cache);
		$this->smarty->setConfigDir($templates);
		
		// Set up Smarty caching
		$this->smarty->setCompileCheck($this->core->Configuration()->GetBool('smarty_template_compile_check', TRUE));
	}
	
	/**
	 * Get the instance of the Smarty itself
	 *
	 * @return Smarty
	 */
	public function Smarty() : Smarty
	{
		return $this->smarty;
	}
	
	/**
	 * Display a template
	 *
	 * @param string $template
	 *    The template filename to display
	 *
	 * @throws FileNotFound
	 *    If the template file is not found
	 * @throws SmartyException
	 *    In case of Smarty errors
	 */
	public function Display(string $template) : void
	{
		// Check that the template exists
		if (!$this->smarty->templateExists($template))
		{
			throw new FileNotFound($template);
		}
		
		$this->smarty->display($template);
	}
	
	/**
	 * Fetch the template contents as string
	 *
	 * @param string $template
	 *    The template filename to fetch
	 *
	 * @return string
	 *    The template contents as a string
	 *
	 * @throws FileNotFound
	 *     If the template file is not found
	 * @throws SmartyException
	 *    In case of Smarty errors
	 */
	public function Fetch(string $template) : string
	{
		// Check that the template exists
		if (!$this->smarty->templateExists($template))
		{
			throw new FileNotFound($template);
		}
		
		return $this->smarty->fetch($template);
	}
	
	/**
	 * A helper method to create a Smarty object from a template
	 * file if the file exists.
	 *
	 * @param string $template
	 *    The template filename to fetch
	 * @param bool|Smarty_Internal_Template $parent
	 *    boolean TRUE to use the main Smarty as the parent,
	 *    boolean FALSE to not set the parent
	 *    Smarty object to use given object as a parent
	 *
	 * @return Smarty_Internal_Template
	 *    Instance of the created Smarty object
	 *
	 * @throws FileNotFound
	 *      If the template file is not found
	 * @throws SmartyException
	 *     In case of Smarty errors
	 */
	public function Create(string $template, bool|Smarty_Internal_Template $parent = TRUE) : Smarty_Internal_Template
	{
		// Check that the template exists
		if (!$this->smarty->templateExists($template))
		{
			throw new FileNotFound($template);
		}
		
		// Check if we want to have a parent object
		if ($parent === FALSE)
		{
			return $this->smarty->createTemplate($template);
		}
		
		// Check for parent class
		if ($parent === TRUE)
		{
			/**
			 * Parent is an optional parameter. It is an uplink to the main Smarty object,
			 * a user-created data object or to another user-created template object
			 *
			 * As such, we can use the Smarty object here, so ignoring the type inspection
			 *
			 * @noinspection CallableParameterUseCaseInTypeContextInspection
			 * @noinspection UnknownInspectionInspection
			 */
			$parent = $this->smarty;
		}
		
		return $this->smarty->createTemplate($template, $parent);
	}
}